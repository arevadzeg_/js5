// 1 - 1
function isFriend(array) {
  let friends = array.filter((friend) => friend.length == 4);
  return friends;
}

// 1 - 2

function isFriend2(array) {
  friends = [];
  for (let element = 0, index = 0; array.length > element; element++) {
    if (array[element].length == 4) {
      friends[index] = array[element];
      index++;
    }
  }
  return friends;
}

friends = ["James", "Will", "Jack", "Nate", "Edward"];

console.log(isFriend(friends));
console.log(isFriend2(friends));

//2-1

const arrey = [3, 87, 45, 12, 7, 3.5, -2];

function leastSum(arrey) {
  let filteredArrey = arrey.filter((a) => a >= 0 && Number.isInteger(a));
  if (filteredArrey.length >= 4) {
    let sortedArrey = filteredArrey.sort((a, b) => a - b);
    return sortedArrey[0] + sortedArrey[1];
  }
  return "Arrey is too short";
}

//2-2
function leastSum2(arrey) {
  const sortedArrey = [...arrey];
  for (let i = 0; i < sortedArrey.length; i++) {
    if (sortedArrey[i] !== Math.floor(sortedArrey[i]) || sortedArrey[i] < 0) {
      sortedArrey.splice(i, 1);
      i--;
    }
  }
  if (sortedArrey.length > 4) {
    let loop = true;
    while (loop) {
      loop = false;
      for (let i = 1; i < arrey.length; i++) {
        if (sortedArrey[i] < sortedArrey[i - 1]) {
          let temp = sortedArrey[i];
          sortedArrey[i] = sortedArrey[i - 1];
          sortedArrey[i - 1] = temp;
          loop = true;
        }
      }
    }
    return sortedArrey[0] + sortedArrey[1];
  }
  return "Arrey is too short";
}

//2-3
function leastSum3(arrey) {
  let arreyClone = arrey.filter((a) => a >= 0 && Number.isInteger(a));

  if (arreyClone.length >= 4) {
    let least1 = Math.min(...arreyClone);
    arreyClone.splice(arreyClone.indexOf(least1), 1);
    let least2 = Math.min(...arreyClone);
    return least1 + least2;
  }
  return "Arrey is too short";
}

console.log(leastSum(arrey));
console.log(leastSum2(arrey));
console.log(leastSum3(arrey));
